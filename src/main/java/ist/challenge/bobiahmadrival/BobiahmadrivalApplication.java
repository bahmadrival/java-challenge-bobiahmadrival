package ist.challenge.bobiahmadrival;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BobiahmadrivalApplication {

	public static void main(String[] args) {
		SpringApplication.run(BobiahmadrivalApplication.class, args);
	}

}
